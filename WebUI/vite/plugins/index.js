import vue from "@vitejs/plugin-vue";

import createAutoImport from "./auto-import";
import createSvgIcon from "./svg-icon";
import createCompression from "./compression";
import createSetupExtend from "./setup-extend";
import monacoEditorPlugin from "vite-plugin-monaco-editor";

export default function createVitePlugins(viteEnv, isBuild = false) {
  const vitePlugins = [vue()];
  vitePlugins.push(createAutoImport());
  vitePlugins.push(createSetupExtend());
  vitePlugins.push(createSvgIcon(isBuild));
  vitePlugins.push(
    monacoEditorPlugin({
      languageWorkers: [
        "editorWorkerService",
        "css",
        "html",
        "json",
        "typescript",
      ]
      // customWorkers: [
      //   {
      //     label: "csharp",
      //     entry: "vs/basic-languages/csharp/csharp.contribution",
      //   },
      // ],
    })
  );
  isBuild && vitePlugins.push(...createCompression(viteEnv));
  return vitePlugins;
}
