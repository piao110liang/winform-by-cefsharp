# 1.基础语法

详见Razor语法教程（[点我打开](https://learn.microsoft.com/zh-cn/aspnet/core/mvc/views/razor?view=aspnetcore-2.1)）。

# 2.嵌套模板（@Include）

## 2.1 基本用法

@Include(string name, object model = null)
参数：name   模板名称 或 相对路径（model/head.html）
参数：model  对象参数，页面参数传递，通过@Model.***使用

```
例子：套用head.html模板

<html>

@Include("head.html")

<body></body>
</html>

```

## 2.2 子模板参数传递

```
例子：套用head.html模板，传递Model

<html>

@Include("head.html",new {
    param1="1",
    param2="2"
})

<body></body>
</html>

```

head.html 页面如下：

```
<head>

@Model.param1  // 即显示：1

@Model.param2  // 即显示：2

</head>
```