import request from "@/utils/request";

/**
 * 创建文件
 *
 * @export
 * @param {*} data
 * @return {*}
 */
export function createFile(data) {
  return request({
    url: "/Template/CreateFile",
    method: "post",
    data: data,
  });
}

/**
 * 创建文件夹
 *
 * @export
 * @param {*} data
 * @return {*}
 */
 export function createFolder(data) {
  return request({
    url: "/Template/CreateFolder",
    method: "post",
    data: data,
  });
}


export function getFiles() {
  return request({
    url: "/Template/GetFiles",
    method: "post",
  });
}

export function deleteFile(data) {
  return request({
    url: "/Template/DeleteFile",
    method: "post",
    data: data,
  });
}

export function saveFile(params) {
  return request({
    url: "/Template/SaveFile",
    method: "post",
    data: params,
  });
}

export function readFile(params) {
  return request({
    url: "/Template/ReadFile",
    method: "post",
    data: params,
  });
}

/**
 * 生成
 *
 * @export
 */
export function generate(params) {
  return request({
    url: "/Template/Generate",
    method: "post",
    data: params,
  });
}
