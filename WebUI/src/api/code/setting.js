import request from "@/utils/request";

/**
 * 保存配置文件
 *
 * @export
 * @param {*} data
 * @return {*}
 */
export function save(data) {
  return request({
    url: "/Setting/Save",
    method: "post",
    data: data,
  });
}

/**
 * 获取配置文件
 *
 * @export
 * @return {*}
 */
export function info() {
  return request({
    url: "/Setting/Config",
    method: "post",
  });
}

/**
 * 获取数据库连接
 *
 * @export
 */
export function getDbOptions() {
  return request({
    url: "/Setting/GetDbOptions",
    method: "post",
  });
}



/**
 * 测试连接
 *
 * @export
 * @return {*} 
 */
export function testConnect(params) {
  return request({
    url: "/Setting/TestConnect",
    method: "post",
    data: params,
  });
}

