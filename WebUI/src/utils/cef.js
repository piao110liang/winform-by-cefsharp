const commonFunction = (excute, ...args) => {
  if (window.razor === undefined) {
    console.error("" + excute + "  请在专属浏览器中运行！！！");
    return new Promise((resolve) => {});
  }
  return window.razor[excute](...args);
};

export const cefServ = {
  /**
   * 选择文件夹弹窗
   *
   * @return {*}
   */
  selectFolder: () => {
    return commonFunction("selectFolder");
  },

  /**
   * 打开文件所在文件夹
   *
   * @param {*} filePath 文件路径
   * @return {*}
   */
  openFolderByFilePath: (filePath) => {
    return commonFunction("openFolderByFilePath", filePath);
  },
  /**
   * 根据文件夹路径，打开文件夹
   *
   * @param {*} folderPath
   * @return {*}
   */
  openFolder: (folderPath) => {
    return commonFunction("openFolder", folderPath);
  },

  /**
   * 文件是否存在
   *
   */
  isExistFile: (filePath) => {
    return commonFunction("isExistFile", filePath);
  },

  /**
   * 根据文件地址 或 文件夹地址，获取Template的相对路径；否则返回空
   *
   * @param {*} path
   * @return {*}
   */
  getTemplateRelativeUrl: (path) => {
    return commonFunction("getTemplateRelativeUrl", path);
  },

  /**
   * 根据地址获取出，相对文件地址
   *
   * @param {*} path
   * @return {*}
   */
  getTemplateRelativeFileUrl: (path) => {
    return commonFunction("getTemplateRelativeFileUrl", path);
  },
};
