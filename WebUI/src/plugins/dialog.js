import Dialog from "./Dialog.vue";
import { h, render } from "vue";

let createMount = (opts) => {
  const mountNode = document.createElement("div");
  document.body.appendChild(mountNode);
  const vnode = h(Dialog, {
    ...opts,
    remove() {
      document.body.removeChild(mountNode);
    },
  });
  vnode.appContext = Modal._context;
  render(vnode, mountNode);
};

const Modal = {
  install(app, options) {
    app.config.globalProperties.$dialog = {
      show: (title, component, componentProps = {}, dialogProps) => {
        // options.id = options.id || "v3popup_" + 1; //唯一id 删除组件时用于定位
        createMount(
          Object.assign(
            {
              title,
              comps: markRaw(defineAsyncComponent(() => component)),
              compsAttr: componentProps,
            },
            dialogProps
          )
        );
      },
    };
  },
  _context: null,
};

export default Modal;
