﻿using CefSharp;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;

namespace CodeGenerateEngine.Models
{
    internal class RequestModel
    {
        private const string CONTENT_TYPE_FORM_URL_ENCODED = "application/x-www-form-urlencoded";
        private const string CONTENT_TYPE_APPLICATION_JSON = "application/json";

        public Uri Uri { get; private set; }

        public string ContentType { get; private set; }

        /// <summary>
        /// Query参数
        /// </summary>
        public NameValueCollection QueryString { get; private set; }

        /// <summary>
        /// Form参数
        /// </summary>
        public NameValueCollection FormData { get; private set; }

        public T GetBodyData<T>()
        {
            if (typeof(T) == typeof(string) || typeof(T) == typeof(String))
            {
                return (T)Convert.ChangeType(this.jsonData, typeof(string));
            }
            return JsonConvert.DeserializeObject<T>(this.jsonData);
        }

        public Encoding ContentEncoding
        {
            get
            {
                var encoding = ContentType;
                if (string.IsNullOrEmpty(encoding) || !encoding.Contains("charset="))
                {
                    encoding = "utf-8";
                }
                else
                {
                    var match = Regex.Match(encoding, @"(?<=charset=)(([^;,\r\n]))*");
                    if (match.Success)
                    {
                        encoding = match.Value;
                    }
                }
                return Encoding.GetEncoding(encoding);
            }
        }

        /// <summary>
        /// 上传文件名称
        /// </summary>
        public string uploadFileName { get; private set; }

        public RequestModel(IRequest Request)
        {
            this.Uri = new Uri(Request.Url);
            this.ContentType = Request.Headers?.Get("Content-Type");
            // 自动解析Query
            ProcessQueryString();
            GetPostData(Request);
            // 自动解析FormData
            if (ContentType != null && ContentType.Contains(CONTENT_TYPE_FORM_URL_ENCODED) && Request.PostData != null)
            {
                FormData = ProcessFormData(this.postData);
            }
            else
            {
                FormData = new NameValueCollection();
            }

            // 自动解析body
            if (this.postData != null && this.postData.Length > 0)
            {
                try
                {
                    jsonData = ContentEncoding.GetString(this.postData);
                }
                catch
                {
                    jsonData = string.Empty;
                }
            }
        }

        public void GetPostData(IRequest Request)
        {
            if (Request != null)
            {
                var items = Request.PostData.Elements;
                if (items != null && items.Count > 0)
                {
                    var bytes = new List<byte>();
                    foreach (var item in items)
                    {

                        var buffer = item.Bytes;
                        switch (item.Type)
                        {
                            case PostDataElementType.Bytes:
                                bytes.AddRange(buffer);
                                break;
                            case PostDataElementType.File:
                                uploadFileName = item.File;
                                break;
                        }
                    }
                    postData = bytes.ToArray();
                }
            }
        }

        private byte[] postData { get; set; }

        private string jsonData { get; set; } = string.Empty;

        private NameValueCollection ProcessFormData(byte[] rawData)
        {

            var query = ContentEncoding.GetString(rawData);
            var retval = new NameValueCollection();
            query = query.Trim('?');
            foreach (var pair in query.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var keyvalue = pair.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (keyvalue.Length == 2)
                {
                    retval.Add(keyvalue[0], Uri.UnescapeDataString(keyvalue[1]));
                }
                else if (keyvalue.Length == 1)
                {
                    retval.Add(keyvalue[0], null);
                }
            }

            return retval;
        }

        private void ProcessQueryString()
        {
            var retval = new NameValueCollection();
            string query = Uri.Query.Trim('?');
            foreach (var pair in query.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var keyvalue = pair.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (keyvalue.Length == 2)
                {
                    retval.Add(keyvalue[0], Uri.UnescapeDataString(keyvalue[1]));
                }
                else if (keyvalue.Length == 1)
                {
                    retval.Add(keyvalue[0], null);
                }
            }
            this.QueryString = retval;
        }

    }
}
