﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Models
{
    /// <summary>
    /// 选择项
    /// </summary>
    internal class SelectOptions
    {

        public string Label { get; set; }

        public string Value { get; set; }

        public string Note { get; set; }
    }
}
