﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Models
{
    internal class SQLDbContext
    {
        private SqlSugarClient DbContext { get; set; }

        private string DbType { get; set; }

        private string ConnectionString { get; set; }

        private SqlSugar.DbType GetDbTypeEnum()
        {
            if (this.DbType.ToLower() == Models.DbType.MySql.ToString().ToLower())
            {
                return SqlSugar.DbType.MySql;
            }
            else if (this.DbType.ToLower() == Models.DbType.SqlServer.ToString().ToLower())
            {
                return SqlSugar.DbType.SqlServer;
            }
            return SqlSugar.DbType.Custom;
        }

        public SQLDbContext(string _DbType, string _connectionString)
        {
            this.DbType = _DbType;
            this.ConnectionString = _connectionString;
        }

        public SqlSugarClient DB
        {
            get
            {
                if (DbContext == null)
                {
                    if (this.GetDbTypeEnum() == SqlSugar.DbType.Custom)
                    {
                        throw new Exception($"{this.DbType} 数据库连接暂不支持！");
                    }
                    DbContext = new SqlSugarClient(new ConnectionConfig()
                    {
                        DbType = this.GetDbTypeEnum(),
                        ConnectionString = this.ConnectionString,
                        IsAutoCloseConnection = true,
                        //IsShardSameThread = true,//设为true相同线程是同一个SqlConnection,是否夸类事务
                        InitKeyType = InitKeyType.Attribute,//mark
                        ConfigureExternalServices = new ConfigureExternalServices()
                        {
                            //DataInfoCacheService = new HttpRuntimeCache()
                        },
                        MoreSettings = new ConnMoreSettings()
                        {
                            IsAutoRemoveDataCache = true
                        }
                    });
                }
                return DbContext;
            }
        }

        public static SQLDbContext GetDefaultDb()
        {
            var conf = Serv.ConfigHelper.GetInstance();
            return new SQLDbContext(conf.DbConf.DbType, conf.DbConf.ConnectionStr);
        }
    }
}
