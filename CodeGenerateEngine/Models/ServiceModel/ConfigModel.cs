﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Models.ServiceModel
{
    internal class ConfigModel
    {
        /// <summary>
        /// 生成路径
        /// </summary>
        public string GenerateDirectory { get; set; }

        /// <summary>
        /// 数据库配置
        /// </summary>
        public DbConfig DbConf { get; set; } = new DbConfig();

    }

    class DbConfig
    {
        /// <summary>
        /// 数据库类型：mysql sqlServer
        /// </summary>
        public string DbType { get; set; } = string.Empty;

        /// <summary>
        /// 字符串链接
        /// </summary>
        public string ConnectionStr { get; set; } = string.Empty;
    }
}
