﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Models.ServiceModel
{
    /// <summary>
    /// 模板文件
    /// </summary>
    internal class TemplateFile
    {
        public string id { get; set; } = string.Empty;

        /// <summary>
        /// 文件名称
        /// </summary>
        public string name { get; set; } = string.Empty;

        /// <summary>
        /// 文件地址
        /// </summary>
        public string path { get; set; } = string.Empty;

        /// <summary>
        /// 文件类型
        /// 文件 为 0；文件夹 为 1；
        /// </summary>
        public int type { get; set; } = 0;

        /// <summary>
        /// 子节点
        /// </summary>
        public List<TemplateFile> children = new List<TemplateFile>();

        /// <summary>
        /// 父节点，相对地址（仅保存，传参使用）
        /// </summary>
        public string parentRelativeUrl { get; set; } = string.Empty;

        public string content { get; set; } = string.Empty;
    }
}
