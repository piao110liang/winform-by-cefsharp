﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Models
{
    internal enum ResponseStatus
    {
        success = 1,
        error = -1
    }

    internal enum DbType
    {
        MySql,
        SqlServer
    }
}
