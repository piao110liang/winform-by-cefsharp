﻿using CefSharp;
using CodeGenerateEngine.Models.ServiceModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CodeGenerateEngine.Models;

namespace CodeGenerateEngine.Serv.System
{
    internal class SettingService : DataService
    {
        /// <summary>
        /// 测试使用
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public IResourceHandler Hi(IRequest Request)
        {
            return TextContent("Hello world!");
        }

        /// <summary>
        /// 系统配置-保存
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public IResourceHandler Save(IRequest Request)
        {
            try
            {
                var WebModel = GetRequest(Request);

                ConfigModel model = WebModel.GetBodyData<ConfigModel>();
                ConfigHelper.SaveConfig(model);
                ConfigHelper.Reset();
                return Success("保存成功");
            }
            catch (Exception ex)
            {
                return Error("保存失败", ex);
            }
        }

        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public IResourceHandler Config(IRequest Request)
        {
            try
            {
                ConfigModel model = ConfigHelper.GetInstance();
                return Success("", model);
            }
            catch (Exception ex)
            {
                Common.LogHelper.Error("获取系统配置失败", ex);
                return Error("获取失败", ex);
            }
        }

        /// <summary>
        /// 获取数据库类型
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public IResourceHandler GetDbOptions(IRequest Request)
        {
            List<SelectOptions> opts = new List<SelectOptions>();
            opts.Add(new SelectOptions()
            {
                Label = "MySql数据库",
                Value = DbType.MySql.ToString(),
                Note = "例子：Server=192.168.0.1;Port=3306;Uid=root;Pwd=123456;"
            });
            opts.Add(new SelectOptions()
            {
                Label = "SqlServer数据库",
                Value = DbType.SqlServer.ToString(),
                Note = "例子：Data Source=192.168.0.1;User ID=sa;Password=123456;MultipleActiveResultSets=True;"
            });
            return Success("", opts);
        }

        /// <summary>
        /// 测试连接
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public IResourceHandler TestConnect(IRequest Request)
        {
            try
            {
                var WebModel = GetRequest(Request);
                DbConfig model = WebModel.GetBodyData<DbConfig>();
                var Context = new SQLDbContext(model.DbType, model.ConnectionStr);
                Context.DB.Ado.CheckConnection();
                return Success();
            }
            catch (Exception ex)
            {
                Common.LogHelper.Error("连接失败", ex);
                return Error("连接失败", ex);
            }
        }

    }
}
