﻿using CefSharp;
using CodeGenerateEngine.Common;
using CodeGenerateEngine.Models.ServiceModel;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CodeGenerateEngine.Serv.System
{
    internal class TemplateService : DataService
    {
        private const string TemplateContent = "@using static CodeGenerateEngine.Serv.RazorServ;";

        public IResourceHandler GetFiles(IRequest Request)
        {
            List<TemplateFile> FileList = GetTemplateFileList(PathUtils.TemplateDirectory);
            return Success("", FileList);
        }

        private List<TemplateFile> GetTemplateFileList(string path)
        {
            List<TemplateFile> result = new List<TemplateFile>();
            // 1.搜索文件夹，并循环添加
            var folders = Directory.GetDirectories(path);
            if (folders != null && folders.Count() > 0)
            {
                foreach (string foldPath in folders)
                {
                    var modelFolder = new TemplateFile();
                    modelFolder.name = Path.GetFileName(foldPath);
                    modelFolder.id = IOUtils.CalculateMD5Hash(modelFolder.name);
                    modelFolder.path = foldPath;
                    modelFolder.type = 1;

                    var child = GetTemplateFileList(foldPath);
                    if (child.Count > 0)
                    {
                        modelFolder.children = child;
                    }
                    result.Add(modelFolder);
                }
            }
            // 2.搜索文件，并循环添加
            string[] files = Directory.GetFiles(path);
            if (files != null && files.Count() > 0)
            {
                foreach (string filePath in files)
                {
                    var modelFolder = new TemplateFile();
                    modelFolder.name = Path.GetFileName(filePath);
                    modelFolder.id = IOUtils.CalculateMD5Hash(Path.GetRelativePath(PathUtils.TemplateDirectory, filePath));
                    modelFolder.path = filePath;
                    modelFolder.type = 0;
                    modelFolder.children = null;
                    result.Add(modelFolder);
                }
            }
            return result;
        }

        public IResourceHandler CreateFile(IRequest Request)
        {
            try
            {
                var model = GetRequest(Request);
                TemplateFile file = model.GetBodyData<TemplateFile>();
                string saveFilePath = string.Empty;
                if (file.parentRelativeUrl == string.Empty)
                {
                    saveFilePath = Path.Combine(PathUtils.TemplateDirectory, file.name);
                }
                else
                {
                    string parentFolder = Path.Combine(PathUtils.TemplateDirectory, file.parentRelativeUrl);
                    IOUtils.CreatDirectory(parentFolder);
                    saveFilePath = Path.Combine(parentFolder, file.name);
                }
                if (File.Exists(saveFilePath))
                {
                    return Error($"{Path.GetFileName(saveFilePath)} 已存在，请勿重复创建。");
                }
                IOUtils.WriteTxt(TemplateContent, saveFilePath);
                return Success("创建成功");
            }
            catch (Exception ex)
            {
                LogHelper.Error("创建失败", ex);
                return Error("创建失败", ex);
            }
        }

        public IResourceHandler CreateFolder(IRequest Request)
        {
            try
            {
                var model = GetRequest(Request);
                TemplateFile file = model.GetBodyData<TemplateFile>();
                //string saveFolderPath = Path.Combine(PathUtils.TemplateDirectory, file.name);
                string saveFolderPath = string.Empty;
                if (file.parentRelativeUrl == string.Empty)
                {
                    saveFolderPath = Path.Combine(PathUtils.TemplateDirectory, file.name);
                }
                else
                {
                    string parentFolder = Path.Combine(PathUtils.TemplateDirectory, file.parentRelativeUrl);
                    IOUtils.CreatDirectory(parentFolder);
                    saveFolderPath = Path.Combine(parentFolder, file.name);
                }

                if (Directory.Exists(saveFolderPath))
                {
                    return Error($"{Path.GetFileName(saveFolderPath)} 已存在，请勿重复创建。");
                }
                Directory.CreateDirectory(saveFolderPath);
                return Success("创建成功");
            }
            catch (Exception ex)
            {
                LogHelper.Error("创建失败", ex);
                return Error("创建失败", ex);
            }
        }

        public IResourceHandler DeleteFile(IRequest Request)
        {
            var model = GetRequest(Request);
            TemplateFile deleteFile = model.GetBodyData<TemplateFile>();
            try
            {
                if (deleteFile.type == 0)
                {
                    string deleteFilePath = deleteFile.path;
                    if (!File.Exists(deleteFilePath))
                    {
                        return Error($"{Path.GetFileName(deleteFilePath)} 不存在。");
                    }
                    IOUtils.DeleteFileToRecycle(deleteFilePath);
                }
                else
                {
                    string deleteFolderPath = deleteFile.path;
                    if (!Directory.Exists(deleteFolderPath))
                    {
                        return Error($"{Path.GetFileName(deleteFolderPath)} 不存在。");
                    }
                    IOUtils.DeleteFolderToRecycle(deleteFolderPath);
                }

                return Success();
            }
            catch (Exception ex)
            {
                LogHelper.Error("删除失败", ex);
                return Error("删除失败", ex);
            }
        }

        public IResourceHandler SaveFile(IRequest Request)
        {
            var model = GetRequest(Request);
            TemplateFile file = model.GetBodyData<TemplateFile>();
            string saveFilePath = file.path;
            if (!File.Exists(saveFilePath))
            {
                return Error($"未找到 {Path.GetFileName(saveFilePath)} 文件");
            }

            IOUtils.WriteTxt_Override(file.content, saveFilePath);
            return Success();
        }

        public IResourceHandler ReadFile(IRequest Request)
        {
            var model = GetRequest(Request);
            TemplateFile file = model.GetBodyData<TemplateFile>();
            string saveFilePath = file.path;
            if (!File.Exists(saveFilePath))
            {
                return Error($"未找到 {Path.GetFileName(saveFilePath)} 文件");
            }

            string content = IOUtils.ReadContent(saveFilePath);
            return Success("", new
            {
                content = content
            });
        }

        public IResourceHandler Generate(IRequest Request)
        {
            try
            {
                var model = GetRequest(Request);
                TemplateFile file = model.GetBodyData<TemplateFile>();
                // 1.保存文件
                string saveFilePath = file.path;
                if (!File.Exists(saveFilePath))
                {
                    return Error($"保存失败，未找到 {Path.GetFileName(saveFilePath)} 文件");
                }
                IOUtils.WriteTxt_Override(file.content, saveFilePath);

                // 2.生成文件
                var instance = RazorHelper.GetInstance();
                // 生成新路径
                var config = ConfigHelper.GetInstance();
                if (config.GenerateDirectory == null || string.IsNullOrEmpty(config.GenerateDirectory))
                {
                    return Error("生成失败。生成路径无效，请在系统配置中重新选择根目录。");
                }
                // 替换路径
                file.path = Path.Combine(config.GenerateDirectory, Path.GetRelativePath(PathUtils.TemplateDirectory, file.path));
                instance.Generate(file.content, file.path);
                return Success(file.path);
            }
            catch (RazorEngine.Templating.TemplateCompilationException ex)
            {
                LogHelper.Error("Razor生成失败", ex);
                return Error("Razor生成失败", new
                {
                    CompilerErrors = ex.CompilerErrors.ToList(),
                    Message = ex.Message,
                });
            }
            catch (Exception ex)
            {
                LogHelper.Error("Razor生成失败", ex);
                return Error("Razor生成失败", ex);
            }
        }
    }
}
