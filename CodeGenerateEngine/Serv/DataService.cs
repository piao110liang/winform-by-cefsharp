﻿using CefSharp;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using CodeGenerateEngine.Models;

namespace CodeGenerateEngine.Serv
{
    internal abstract class DataService
    {
        /// <summary>
        /// IRequest 格式化 RequestModel对象
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        protected static RequestModel GetRequest(IRequest request)
        {
            return new RequestModel(request);
        }

        protected static IResourceHandler Success(string Msg = "", object obj = null)
        {
            return JsonContent(new ResponseModel()
            {
                Code = (int)ResponseStatus.success,
                Message = Msg,
                Data = obj
            });
        }

        protected static IResourceHandler Error(string Msg = "", object obj = null)
        {
            return JsonContent(new ResponseModel()
            {
                Code = (int)ResponseStatus.error,
                Message = Msg,
                Data = obj
            });
        }

        protected static IResourceHandler JsonContent(object data, Newtonsoft.Json.JsonSerializerSettings jsonSerializerSettings = null)
        {
            var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data, jsonSerializerSettings));
            var result = ResourceHandler.FromStream(new MemoryStream(bytes), "application/json");
            result.StatusCode = 200;
            return result;
        }

        protected static IResourceHandler JsonContent<T>(T data, Newtonsoft.Json.JsonSerializerSettings jsonSerializerSettings = null)
        {
            var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data, jsonSerializerSettings));
            var result = ResourceHandler.FromStream(new MemoryStream(bytes), "application/json");
            result.StatusCode = 200;
            return result;
        }

        protected static IResourceHandler TextContent(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var result = ResourceHandler.FromStream(new MemoryStream(bytes), "text/plain");
            result.StatusCode = 200;
            return result;
        }

        protected static IResourceHandler TextContent(string text, Encoding encoding)
        {
            var bytes = encoding.GetBytes(text);
            var result = ResourceHandler.FromStream(new MemoryStream(bytes), "text/plain");
            result.StatusCode = 200;
            return result;
        }
    }
}
