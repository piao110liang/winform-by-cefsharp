﻿using System;
using System.Collections.Generic;
using System.Text;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using RazorEngine.Text;
using System.IO;
using System.Linq;
using CodeGenerateEngine.Common;
using SqlSugar;

namespace CodeGenerateEngine.Serv
{
    /// <summary>
    /// Razor代码生成辅助类
    /// 
    /// github源码地址：https://github.com/Antaris/RazorEngine
    /// 中文教程地址：https://www.cnblogs.com/sunjie9606/p/6807326.html
    /// 工作中的Monaco Editor使用经验总结：https://segmentfault.com/a/1190000042292716
    /// </summary>
    internal class RazorHelper
    {
        public static RazorHelper GetInstance()
        {
            return new RazorHelper();
        }

        private ITemplateKey key { get; set; }

        private IRazorEngineService service { get; set; }

        private string[] ResolePaths { get; set; }

        private RazorHelper()
        {
            Init();
        }

        private string[] GetTemplate_RootDir()
        {
            if (ResolePaths != null && ResolePaths.Length > 0)
            {
                return this.ResolePaths;
            }
            else
            {
                return new string[] { PathUtils.TemplateDirectory };
            }
        }

        private void Init()
        {
            TemplateServiceConfiguration configuration = new TemplateServiceConfiguration();
            configuration.Language = Language.CSharp;
            configuration.TemplateManager = new DelegateTemplateManager((string key) =>
            {
                // 定义委托
                var templatePath = ResolveFilePath(GetTemplate_RootDir(), key);
                string templateContent = IOUtils.ReadContent(templatePath);
                return templateContent;
            });
            service = RazorEngineService.Create(configuration);
            key = service.GetKey(Guid.NewGuid().ToString());
        }

        /// <summary>
        /// 动态解析文件路径
        /// </summary>
        /// <param name="layoutRoots"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        private string ResolveFilePath(string[] layoutRoots, string name)
        {
            if (File.Exists(name))
            {
                return name;
            }

            foreach (var root in layoutRoots)
            {
                var path = Path.Combine(root, name);

                if (File.Exists(path))
                {
                    return path;
                }

                //Check if a file with the csharp extension exists
                var csPath = path + ".cshtml";
                if (File.Exists(csPath))
                {
                    return csPath;
                }

                //Check if a file with the visual basic extension exists
                var vbPath = path + ".vbhtml";
                if (File.Exists(vbPath))
                {
                    return vbPath;
                }
            }
            throw new InvalidOperationException(string.Format("无法找到 {0} 模板文件", name));
        }

        /// <summary>
        /// 设置 解析根目录
        /// </summary>
        public void SetResolePaths(string[] ResolePath)
        {
            this.ResolePaths = ResolePath;
        }

        /// <summary>
        /// 生成
        /// </summary>
        public void Generate(string templateContent, string SaveFilePath, object model = null, DynamicViewBag viewBag = null)
        {
            string saveDir = Path.GetDirectoryName(SaveFilePath);
            Common.IOUtils.CreatDirectory(saveDir);
            service.AddTemplate(key, templateContent);
            service.Compile(key);
            string generateContent = service.Run(key, model: model, viewBag: viewBag);// 生成的内容
            // 保存至文件
            if (File.Exists(SaveFilePath))
            {
                Common.IOUtils.WriteTxt_Override(generateContent, SaveFilePath);
            }
            else
            {
                Common.IOUtils.WriteTxt(generateContent, SaveFilePath);
            }
        }

    }
}
