﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using System.IO;
using CodeGenerateEngine.Common;
using RazorEngine.Templating;
using SqlSugar;
using System.Linq;
using RazorEngine.Text;

namespace CodeGenerateEngine.Serv
{
    public static class RazorServ
    {
        /// <summary>
        /// 获取 guid
        /// </summary>
        /// <returns></returns>
        public static string GetID()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Template文件夹
        /// </summary>
        public static string TemplateRoot
        {
            get
            {
                return PathUtils.TemplateDirectory;
            }
        }

        /// <summary>
        /// 使用 Template 模板，生成输出代码
        /// </summary>
        /// <param name="TemplatePath">模板路径</param>
        /// <param name="SaveFileName">保留文件地址（注：相对地址）</param>
        /// <param name="model">（可选）对象数据</param>
        /// <param name="viewBag">（可选）viewBag数据</param>
        /// <exception cref="ArgumentException"></exception>
        public static void GenerateUseTemplate(string TemplatePath, string SaveFileName = null, object model = null, dynamic viewBag = null)
        {
            // 1.判断模板文件是否存在
            string TemplateFilePath = Path.Combine(PathUtils.TemplateDirectory, TemplatePath);
            if (!File.Exists(TemplateFilePath))
            {
                throw new ArgumentException($"{TemplateFilePath} 不存在");
            }
            // 2.获取模板内容
            string templateContent = IOUtils.ReadContent(TemplateFilePath);
            // 获取保存路径
            string relativePath = Path.GetDirectoryName(TemplatePath); // 相对位置
            string RootPATH = ConfigHelper.GetInstance().GenerateDirectory;
            string saveFilePath = Path.Combine(RootPATH, relativePath, SaveFileName.TrimStart('/').TrimStart('\\')); // 生成根目录/相对位置/文件名名称

            // 3.创建Razor对象
            var instance = RazorHelper.GetInstance();
            DynamicViewBag dyModel = Newtonsoft.Json.JsonConvert.DeserializeObject<DynamicViewBag>(Newtonsoft.Json.JsonConvert.SerializeObject(viewBag));
            //dynamic dyModel = new DynamicViewBag();
            //dyModel = viewBag;
            instance.Generate(templateContent, saveFilePath, model, dyModel);
        }

        #region 格式化转换相关

        /// <summary>
        /// 对象转格式化
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJson(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        #endregion

        #region 数据库连接相关

        /// <summary>
        /// 获取数据表
        /// </summary>
        /// <returns>返回所有表名称数组，string[]</returns>
        public static string[] GetDbTables()
        {
            var DbContext = Models.SQLDbContext.GetDefaultDb();
            var tableList = DbContext.DB.DbMaintenance.GetTableInfoList(false);
            DbContext.DB.Close();
            return tableList.Select(p => p.Name).ToArray();
        }

        /// <summary>
        /// 根据表名获取列信息
        /// </summary>
        /// <param name="TableName">表名称</param>
        /// <returns>返回 DbColumnInfo 集合</returns>
        public static List<DbColumnInfo> GetDbTableColumns(string TableName)
        {
            var DbContext = Models.SQLDbContext.GetDefaultDb();
            var columnList = DbContext.DB.DbMaintenance.GetColumnInfosByTableName(TableName);
            DbContext.DB.Close();
            return columnList;

        }

        /// <summary>
        /// 使用Sql语句
        /// </summary>
        /// <param name="selectSql"></param>
        public static void UseSql(string selectSql)
        {

        }


        #endregion

    }
}
