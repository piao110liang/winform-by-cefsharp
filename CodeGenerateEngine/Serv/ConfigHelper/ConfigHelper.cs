﻿using CodeGenerateEngine.Common;
using CodeGenerateEngine.Models.ServiceModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Serv
{
    internal class ConfigHelper
    {
        private static ConfigModel _instance { get; set; }

        /// <summary>
        /// 获取系统配置信息
        /// </summary>
        /// <returns></returns>
        public static ConfigModel GetInstance()
        {
            if (_instance == null)
            {
                string content = IOUtils.ReadContent(PathUtils.ConfigJsonPath);
                _instance = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfigModel>(content);
            }
            return _instance;
        }

        /// <summary>
        /// 重置
        /// </summary>
        public static void Reset()
        {
            _instance = null;
            GetInstance();
            // 重置Appsettings
            Appsettings.Reset();
        }

        /// <summary>
        /// 保存配置
        /// </summary>
        /// <param name="model"></param>
        public static void SaveConfig(ConfigModel model)
        {
            JObject obj = Appsettings.Merge(model);
            string content = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            IOUtils.WriteTxt_Override(content, PathUtils.ConfigJsonPath);
        }
    }
}
