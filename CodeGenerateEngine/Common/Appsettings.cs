﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CodeGenerateEngine.Common;
using CodeGenerateEngine.Models.ServiceModel;

namespace CodeGenerateEngine.Common
{
    internal class Appsettings
    {
        static JObject Configuration { get; set; }

        /// <summary>
        /// 读取配置文件
        /// </summary>
        public static void Build()
        {
            string content = IOUtils.ReadContent(PathUtils.ConfigJsonPath);
            JObject obj = JsonConvert.DeserializeObject<JObject>(content);
            Configuration = obj;
        }

        public static void Reset()
        {
            Build();
        }

        public static string Get(params string[] sections)
        {
            try
            {
                JObject temp = Configuration;
                string result = string.Empty;
                for (int i = 0; i < sections.Length; i++)
                {
                    var val = temp[sections[i]];
                    if (val.Type == JTokenType.Object)
                    {
                        temp = val.Value<JObject>();
                    }
                    else
                    {
                        result = val.Value<string>();
                        break;
                    }
                }
                if (result == string.Empty)
                {
                    return string.Empty;
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info($"{string.Join(":", sections)}读取配置失败", ex);
                return string.Empty;
            }
        }

        public static JObject Merge(ConfigModel model)
        {
            JObject obj2 = JObject.FromObject(model);
            Configuration.Merge(obj2);
            return Configuration;
        }
    }
}
