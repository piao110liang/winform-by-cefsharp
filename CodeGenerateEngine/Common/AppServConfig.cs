﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Common
{
    internal class AppServConfig
    {
        /// <summary>
        /// 静态文件虚拟根地址
        /// </summary>
        public const string StaticFileDirectory = "http://static.local.com";

        /// <summary>
        /// 本地api服务根地址
        /// </summary>
        public const string servAPI = "http://api.local.com";

        /// <summary>
        /// api接口服务，后缀标识
        /// </summary>
        public const string SERVICE_SUFFIX = "Service";
    }
}
