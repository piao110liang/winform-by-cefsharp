﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;


namespace CodeGenerateEngine.Common
{
    internal class IOUtils
    {
        public const string ExplorerFilePath = @"C:\Windows\explorer.exe";

        /// <summary>
        /// 按行读取文件所有文本 UTF8
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string ReadContent(string filePath)
        {
            return ReadContent(filePath, new System.Text.UTF8Encoding(false));
        }

        /// <summary>
        /// 按行读取文件所有文本，制定编码格式
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string ReadContent(string filePath, Encoding encoding)
        {
            if (File.Exists(filePath))
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader sr = new StreamReader(fs, encoding))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// 写文本文件
        /// </summary>
        /// <param name="Str"></param>
        public static void WriteTxt(string Str, string FilePath)
        {
            WriteTxt(Str, FilePath, true);   //默认追加文本
        }

        public static void WriteTxt_Override(string Str, string FilePath)
        {
            WriteTxt(Str, FilePath, false, new System.Text.UTF8Encoding(false));   //默认UTF8保存
        }

        /// <summary>
        /// 写文本文件
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="isAppend">是否追加</param>
        public static void WriteTxt(string Str, string FilePath, bool isAppend)
        {
            WriteTxt(Str, FilePath, isAppend, new System.Text.UTF8Encoding(false));   //默认UTF8保存
        }

        /// <summary>
        /// 写文本文件
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="isAppend">是否追加</param>
        public static void WriteTxt(string Str, string FilePath, bool isAppend, Encoding strEncode)
        {
            FileStream fsapp;
            if (File.Exists(FilePath))
            {
                if (isAppend)
                    fsapp = new FileStream(@FilePath, FileMode.Append, FileAccess.Write);
                else
                {
                    fsapp = new FileStream(@FilePath, FileMode.Open, FileAccess.Write);
                    fsapp.SetLength(0);   //清空文件内容
                }
            }
            else
            {
                fsapp = new FileStream(@FilePath, FileMode.Create);
            }
            StreamWriter sw = new StreamWriter(fsapp, strEncode);
            sw.WriteLine(Str);
            sw.Flush();
            sw.Close();
            fsapp.Close();
        }

        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public static bool CreatDirectory(string directory)
        {
            //判断文件夹是否存在
            if (Directory.Exists(directory))
            {
                return true;
            }
            try
            {
                Directory.CreateDirectory(directory);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        /// <summary>
        /// 打开文件夹
        /// </summary>
        /// <param name="directoryPath"></param>
        public static void OpenFolder(string directoryPath)
        {
            System.Diagnostics.Process.Start(ExplorerFilePath, directoryPath);
        }

        /// <summary>
        /// 打开文件所在文件夹，并选中
        /// </summary>
        /// <param name="filePath"></param>
        public static void OpenFolderAndSelected(string filePath)
        {
            System.Diagnostics.Process.Start(ExplorerFilePath, "/select," + filePath);
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFile(string path)
        {
            File.Delete(path);
        }

        /// <summary>
        /// 删除文件夹
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFolder(string path)
        {
            Directory.Delete(path, true);
        }

        /// <summary>
        /// 删除文件至回收站
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFileToRecycle(string path)
        {
            FileSystem.DeleteFile(path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
        }

        /// <summary>
        /// 删除文件夹至回收站
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFolderToRecycle(string path)
        {
            FileSystem.DeleteDirectory(path, UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin);
        }

        public static string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

    }
}
