﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerateEngine.Common
{
    class LogHelper
    {
        private static LogService _instance = null;

        private static LogService instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LogService();
                }
                return _instance;
            }
        }

        public static void Info(string msg)
        {
            instance.Info(msg);
        }

        public static void Info(string msg, Exception ex)
        {
            instance.Info(msg, ex);
        }

        public static void Error(string msg)
        {
            instance.Error(msg);
        }

        public static void Error(string msg, Exception ex)
        {
            instance.Error(msg, ex);
        }
    }
}
