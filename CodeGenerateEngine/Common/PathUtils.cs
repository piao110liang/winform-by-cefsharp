﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CodeGenerateEngine.Common
{
    internal static class PathUtils
    {

        /// <summary>
        /// 获取当前程序集所在路径文件夹
        /// </summary>
        public static string AppDirectory
        {
            get
            {
                return Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);
            }
        }

        /// <summary>
        /// Source文件夹
        /// </summary>
        public static string AppSourceDirectory
        {
            get
            {
                return Path.Combine(AppDirectory, "Source");
            }
        }

        /// <summary>
        /// config.json文件
        /// </summary>
        public static string ConfigJsonPath
        {
            get
            {
                return Path.Combine(AppDirectory, "config.json");
            }
        }

        public static string WebUIDirectory
        {
            get
            {
                string path = Path.Combine(AppSourceDirectory, "WebUI");
                IOUtils.CreatDirectory(path);
                return path;
            }
        }

        /// <summary>
        /// 模板文件-文件夹
        /// </summary>
        public static string TemplateDirectory
        {
            get
            {
                string str = Path.Combine(AppSourceDirectory, "Template");
                IOUtils.CreatDirectory(str);
                return str;
            }
        }
    }
}
