using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeGenerateEngine
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ThreadException += Application_ThreadException;
            Application.ApplicationExit += Application_ApplicationExit;
            Common.LogHelper.Info("��������");
            Common.Appsettings.Build();
            GBrowserForm.Init();
            Application.Run(new MainForm());
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Common.LogHelper.Error("Application ThreadException", e.Exception);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Common.LogHelper.Error("CurrentDomain Exception", (Exception)e.ExceptionObject);
            MessageBox.Show("����ʧ�ܣ������־", "����");
        }

        private static void Application_ApplicationExit(object sender, EventArgs e)
        {
            GBrowserForm.Shutdown();
        }
    }
}
