﻿using CefSharp;
using CefSharp.SchemeHandler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CodeGenerateEngine.SchemeFactory
{
    /// <summary>
    /// 本地资源访问服务
    /// </summary>
    internal class ResourceSchemeHandlerFactory
    {
        public static CefCustomScheme GetInstance(string SchemeName, string DomainName, string localFileResourceDirectory)
        {
            return new CefCustomScheme()
            {
                SchemeName = SchemeName,
                DomainName = DomainName,
                SchemeHandlerFactory = new NewFolderSchemeHandlerFactory(rootFolder: localFileResourceDirectory, schemeName: SchemeName, hostName: DomainName)
            };
        }

        /// <summary>
        /// 重载FolderSchemeHandlerFactory
        /// </summary>
        public class NewFolderSchemeHandlerFactory : FolderSchemeHandlerFactory
        {
            private FileShare NewresourceFileShare { get; set; }

            private string NewrootFolder { get; set; }

            public NewFolderSchemeHandlerFactory(string rootFolder, string schemeName = null, string hostName = null, string defaultPage = "index.html", FileShare resourceFileShare = FileShare.Read) : base(rootFolder, schemeName, hostName, defaultPage, resourceFileShare)
            {
                this.NewresourceFileShare = resourceFileShare;
                this.NewrootFolder = rootFolder;
            }

            protected override IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
            {
                var baseResult = base.Create(browser, frame, schemeName, request);
                if (baseResult is ResourceHandler)
                {
                    var reResult = baseResult as ResourceHandler;
                    if (reResult != null && reResult.StatusCode == (int)System.Net.HttpStatusCode.NotFound)
                    {
                        if (reResult.MimeType == "text/html")
                        {
                            //
                            string filePath = Path.Combine(this.NewrootFolder, "index.html");
                            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, NewresourceFileShare);
                            return ResourceHandler.FromStream(stream, "text/html", autoDisposeStream: true);
                        }
                    }
                }
                return baseResult;
            }
        }
    }
}
