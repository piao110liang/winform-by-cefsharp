﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CodeGenerateEngine
{
    class JavascriptObject
    {
        private GBrowserForm form;

        public JavascriptObject(GBrowserForm _form)
        {
            this.form = _form;
        }

        /// <summary>
        /// 选择文件夹
        /// </summary>
        /// <returns></returns>
        public string selectFolder()
        {
            string FilePath = "";
            Thread InvokeThread = new Thread(() =>
            {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    FilePath = dialog.SelectedPath;
                }
            });
            InvokeThread.SetApartmentState(ApartmentState.STA);
            InvokeThread.Start();
            InvokeThread.Join();
            return FilePath;
        }

        /// <summary>
        /// 打开文件所在文件夹
        /// </summary>
        /// <param name="file">文件路径</param>
        public void openFolderByFilePath(string file)
        {
            Common.IOUtils.OpenFolderAndSelected(file);
        }

        /// <summary>
        /// 根据文件夹路径，打开文件夹
        /// </summary>
        /// <param name="folderPath">文件夹路径</param>
        public void openFolder(string folderPath)
        {
            Common.IOUtils.OpenFolder(folderPath);
        }

        /// <summary>
        /// 根据地址获取出，相对地址
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string getTemplateRelativeUrl(string path)
        {
            if (!path.StartsWith(Common.PathUtils.TemplateDirectory))
            {
                return string.Empty;
            }
            if (System.IO.File.Exists(path))
            {
                string str = System.IO.Path.GetRelativePath(Common.PathUtils.TemplateDirectory, System.IO.Path.GetDirectoryName(path));
                return str.TrimStart('.');
            }
            else
            {
                return System.IO.Path.GetRelativePath(Common.PathUtils.TemplateDirectory, path);
            }
        }

        /// <summary>
        /// 根据地址获取出，相对文件地址
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string getTemplateRelativeFileUrl(string path)
        {
            if (!path.StartsWith(Common.PathUtils.TemplateDirectory))
            {
                return string.Empty;
            }
            if (System.IO.File.Exists(path))
            {
                string str = System.IO.Path.GetRelativePath(Common.PathUtils.TemplateDirectory, path);
                return str.TrimStart('.');
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 是否存在文件
        /// </summary>
        /// <returns></returns>
        public bool isExistFile(string filePath)
        {
            return System.IO.File.Exists(filePath);
        }
    }
}
