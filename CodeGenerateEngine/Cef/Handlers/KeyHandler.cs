﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace CodeGenerateEngine.Handlers
{
    internal class KeyHandler : IKeyboardHandler
    {
        public GBrowserForm host = null;
        private const int VK_ESCAPE = 0x1B;
        private const int VK_F5 = 0x74;
        private const int VK_F12 = 0x7B;

        public KeyHandler(GBrowserForm form)
        {
            host = form;
        }

        public bool OnKeyEvent(IWebBrowser browserControl,
                               IBrowser browser,
                               KeyType type,
                               int windowsKeyCode,
                               int nativeKeyCode,
                               CefEventFlags modifiers,
                               bool isSystemKey)
        {
            if (type == KeyType.KeyUp)
            {
                if (windowsKeyCode == VK_ESCAPE)
                {
                    if (host.Parent == null)
                    {
                        //host.Close();
                    }
                    return true;
                }
                if (windowsKeyCode == VK_F5)
                {
                    browser.Reload();
                    return true;
                }
                if (windowsKeyCode == VK_F12)
                {
                    browser.ShowDevTools();
                    return true;
                }
            }
            return false;
        }

        public bool OnPreKeyEvent(IWebBrowser browserControl,
                                  IBrowser browser,
                                  KeyType type,
                                  int windowsKeyCode,
                                  int nativeKeyCode,
                                  CefEventFlags modifiers,
                                  bool isSystemKey,
                                  ref bool isKeyboardShortcut)
        {
            return false;
        }
    }
}
