﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CodeGenerateEngine
{
    internal partial class MainForm : GBrowserForm
    {
        public MainForm()
        {
            this.Size = new System.Drawing.Size(1024, 800);
            this.Text = "代码生成器";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
#if DEBUG
            // debug环境
            string serHttp = Common.Appsettings.Get("Debug", "HttpServerUrl");
            this.LoadURL(serHttp);
            //this.LoadURL(@"http://static.local.com/");
#else
        // release环境发布
        this.LoadURL(Flurl.Url.Combine(Common.AppServConfig.StaticFileDirectory, "index"));
#endif

        }
    }
}
