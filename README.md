# 基于Razor语法的代码生成器

## 1.平台介绍
一直找简单、实用、可编辑的代码生成器，之前开发项目用T4模板，个人感觉缺点就是复杂代码，阅读效果较差，开发没有效率。在偶然间发现Razor Engine利器，开发net的人对这个东西很熟悉、亲切。于是利用空闲时间开发“基于Razor语法的代码生成器”。

开发技术：Winform + CefSharp 、Vue3 + RuoYi 

![avatar](Images/home.png)

![avatar](Images/code.jpg)

## 2.启动方式
```
CodeGenerateEngine  // 文件夹winform桌面应用程序
WebUI               // 前端页面程序

1.Debug 启动
-- （1）cd WebUI
-- （2）yarn dev // 启动服务 访问：http://localhost:8066/
-- （3）打开CodeGenerateEngine.sln 
-- （4）Ctrl+F5 启动CodeGenerateEngine

2.Release 发布
-- （1）cd WebUI
-- （2）yarn build:prod // 发布到dist目录下
-- （3）CodeGenerateEngine.sln // release 发布
-- （4）把dist文件夹内的文件，拷贝至（发布路径）\Source\WebUI目录下

```
## 3.打包文件使用

<a href="https://download.csdn.net/download/piao110liang/86937457" target="_blank">[下载程序]</a>，解压后打开CodeGenerateEngine.exe

---

# 1.基础语法

详见Razor语法教程（[点我打开](https://learn.microsoft.com/zh-cn/aspnet/core/mvc/views/razor?view=aspnetcore-2.1)）。

# 2.嵌套模板（@Include）

## 2.1 基本用法

@Include(string name, object model = null)
参数：name   模板名称 或 相对路径（model/head.html）
参数：model  对象参数，页面参数传递，通过@Model.***使用

```
例子：套用head.html模板

<html>

@Include("head.html")

<body></body>
</html>

```

## 2.2 子模板参数传递

```
例子：套用head.html模板，传递Model

<html>

@Include("head.html",new {
    param1="1",
    param2="2"
})

<body></body>
</html>

```

head.html 页面如下：

```
<head>

@Model.param1  // 即显示：1

@Model.param2  // 即显示：2

</head>
```